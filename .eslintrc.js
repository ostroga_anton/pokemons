module.exports = {
	'env': {
		'browser': true,
		'es2020': true
	},
	'extends': ['plugin:react/recommended', 'airbnb', 'plugin:react-pug/all'],
	'parser': 'babel-eslint',
	'parserOptions': {
		'ecmaFeatures': {
			'jsx': true,
			'experimentalObjectRestSpread': true
		},
		'ecmaVersion': 12,
		'sourceType': 'module'
	},
	'plugins': [
		'react',
		'react-pug'
	],
	'rules': {
		'semi': ['error', 'never'],
		'quotes': ['error', 'single'],
		'indent': ['error', 'tab'],
		'no-tabs': 0,
		'quote-props': 'off',
		'comma-dangle': ['error', 'never'],
		'import/extensions': 'off',
		'import/no-unresolved': 'off',
		'arrow-body-style': 'off',
		'prefer-const': 'off',
		'react-pug/indent': 'off',
		'react-pug/pug-lint': 'off',
		'react-pug/quotes': 'off',
		'react-pug/empty-lines': 'off',
		'react/jsx-filename-extension': 'off',
		'react/jsx-indent': 'off',
		'react-pug/prop-types': 'off',
		'react-pug/no-interpolation': 'off',
		'no-plusplus': 'off',
		'camelcase': 'off',
		'consistent-return': 'off'
	}
}
