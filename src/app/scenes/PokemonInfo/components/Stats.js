import React from 'react'

const ucFirst = (str) => {
	if (!str) return str

	return str[0].toUpperCase() + str.slice(1)
}

const Stats = ({ data }) => {
	let i = 0

	return pug`
		div.stats-wrapper
			ul Stats:
				while i < data.length
					li(key=${`stat-${i}`})
						span.key ${ucFirst(data[i].name)}
						span.value ${data[i++].value}
	`
}

export default React.memo(Stats)
