import {
	fetchPokemonsData,
	getFilteredPokemons,
	getPokemonsFetchErrorMsg,
	getPokemonsFetchingStatus,
	inputName
} from 'store/features/pokemons'

import { connect } from 'react-redux'
import Main from './component'

const mapStateToProps = (state) => {
	return {
		pokemons: getFilteredPokemons(state),
		fetchErrorMsg: getPokemonsFetchErrorMsg(state),
		isFetching: getPokemonsFetchingStatus(state)
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		onMounted: () => {
			dispatch(fetchPokemonsData())
		},
		onUnmount: () => {
			dispatch(inputName(''))
		},
		onInputName: (e) => {
			dispatch(inputName(e.target.value))
		}
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Main)
