import './style/index.sass'

import { Provider } from 'react-redux'
import React from 'react'
import { HashRouter as Router } from 'react-router-dom'
import { render } from 'react-dom'
import App from './app'
import store from './store'

render(
	<Router>
		<Provider store={store}>
			<App />
		</Provider>
	</Router>,
	document.getElementById('app')
)
