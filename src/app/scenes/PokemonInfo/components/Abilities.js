import React from 'react'
import { Link } from 'react-router-dom'

const Abilities = ({ data }) => {
	let i = 0
	const { normalizedAbilities } = data

	return pug`
		div.abilities-wrapper Abilities:
			while i < normalizedAbilities.length
				li.ability(key=${`ability-${i}`})
					Link(
					key=${`ability-${i}`}
					to=${{ pathname: `/pokemon/${data.id}/ability/${normalizedAbilities[i].id}` }}
				) ${normalizedAbilities[i++].name}
	`
}

export default React.memo(Abilities)
