const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack')

module.exports = {
	entry: __dirname + "/src/index.js",
	output: {
		path: path.resolve(__dirname, 'build'),
		filename: 'bundle.js',
	},
	devServer: {
		contentBase: './',
		port: 7700,
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'babel-loader',
						options: {
							cacheDirectory: true,
							presets: [
								[
									'@babel/preset-env',
									{
										targets: {
											esmodules: true
										}
									}
								]
							],
							plugins: [
								'react-hot-loader/babel',
								'transform-react-pug',
								'transform-react-jsx'
							]
						}
					}
				]
			},
			{
				test: /\.s[ac]ss$/i,
				use: [
					'style-loader',
					'css-loader',
					{
						loader: 'sass-loader',
						options: {
							// Prefer `dart-sass`
							implementation: require('sass'),
						},
					},
				],
			},
			{
				test: /\.pug$/,
				use: {
					loader: 'pug-loader'
				}
			},
		]
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: './index.pug'
		}),
		new webpack.HotModuleReplacementPlugin()
	],
	resolve: {
		extensions: ['.js', '.styl'],
		modules: ['node_modules', 'lib', 'shared'],
		mainFiles: ['index', 'connect', 'component'],
		alias: {
			'store': path.resolve('src', 'store')
		}
	}
};