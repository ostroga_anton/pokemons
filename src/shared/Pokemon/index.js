import React from 'react'

const Pokemon = ({ data, style }) => {
	return pug`
		div.pokemon(data-id=data.id style=style)
			img.pokemon-image(
				src=${`https://pokeres.bastionbot.org/images/pokemon/${data.id}.png`}
				alt='Pokemon'
			)
			div.pokemon-name ${data.name}
	`
}

export default React.memo(Pokemon)
