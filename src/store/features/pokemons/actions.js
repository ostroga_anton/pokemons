import { createActions } from 'redux-actions'

const pokemons = []

export const {
	setPokemonsData,
	inputName,
	setPokemonsFetchingStatus,
	setPokemonsFetchErrorMsg
} = createActions(
	'SET_POKEMONS_DATA',
	'INPUT_NAME',
	'SET_POKEMONS_FETCHING_STATUS',
	'SET_POKEMONS_FETCH_ERROR_MSG'
)

export const fetchPokemonsData = () => async (dispatch) => {
	try {
		dispatch(setPokemonsFetchingStatus(true))

		const response = await fetch('https://pokeapi.co/api/v2/pokemon?offset=0&limit=30')
		const { results } = await response.json()

		for (let i = 0; i < results.length; i += 1) {
			pokemons[i] = {
				name: results[i].name,
				id: i + 1
			}
		}

		dispatch(setPokemonsData(pokemons))
	} catch (error) {
		dispatch(setPokemonsFetchErrorMsg('PokemonsFetchError'))
	} finally {
		dispatch(setPokemonsFetchingStatus(false))
	}
}
