export const getFilteredPokemons = (state) => {
	const { data, input } = state.pokemons

	if (data.length > 0) {
		const allPokemons = data

		const filteredPokemons = allPokemons.filter(({ name }) => {
			return name.toLowerCase().indexOf(input.toLowerCase()) !== -1
		})

		return filteredPokemons
	}
}

export const getPokemonsFetchErrorMsg = (state) => {
	const { fetchErrorMsg } = state.pokemons

	return fetchErrorMsg
}

export const getPokemonsFetchingStatus = (state) => {
	const { isFetching } = state.pokemons

	return isFetching
}
