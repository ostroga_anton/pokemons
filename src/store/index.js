import { applyMiddleware, createStore, combineReducers } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'

import pokemonListReducer from './features/pokemons'
import pokemonReducer from './features/pokemon'
import abilityReducer from './features/ability'

const reducer = combineReducers({
	ability: abilityReducer,
	pokemons: pokemonListReducer,
	pokemon: pokemonReducer
})

export default createStore(reducer, composeWithDevTools(applyMiddleware(thunk)))
