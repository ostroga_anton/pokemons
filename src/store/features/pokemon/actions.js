import { createActions } from 'redux-actions'

export const {
	setPokemonData,
	clearPokemonData,
	setPokemonFetchingStatus,
	setPokemonFetchErrorMsg
} = createActions(
	'SET_POKEMON_DATA',
	'CLEAR_POKEMON_DATA',
	'SET_POKEMON_FETCHING_STATUS',
	'SET_POKEMON_FETCH_ERROR_MSG'
)

export const setCurrentPokemon = (id) => async (dispatch) => {
	try {
		dispatch(setPokemonFetchingStatus(true))

		const response = await fetch(`https://pokeapi.co/api/v2/pokemon/${id}/`)
		const result = await response.json()

		const {
			name,
			types,
			stats,
			abilities
		} = result

		const normalizedStats = []
		const normalizedTypes = []
		const normalizedAbilities = []

		stats.forEach((el) => {
			normalizedStats.push({ name: el.stat.name, value: el.base_stat })
		})

		types.forEach((el) => {
			normalizedTypes.push(el.type.name)
		})

		abilities.forEach((el) => {
			const { url } = el.ability
			const path = url.split('/')
			const abilityId = path[path.length - 2]

			normalizedAbilities.push({ name: el.ability.name, id: abilityId })
		})

		await dispatch(setPokemonData({
			id, name, normalizedTypes, normalizedStats, normalizedAbilities
		}))
	} catch (error) {
		dispatch(setPokemonFetchErrorMsg('setPokemonFetchErrorMsg'))
	} finally {
		dispatch(setPokemonFetchingStatus(false))
	}
}
