import React, { useEffect } from 'react'

import { useRouteMatch } from 'react-router-dom'

import Pokemon from 'Pokemon'
import BackButton from 'BackButton'
import Abilities from './components/Abilities'
import Stats from './components/Stats'
import Types from './components/Types'

const PokemonInfo = ({
	data,
	onPokemonSelected,
	fetchErrorMsg,
	isFetching,
	onUnmount
}) => {
	const {
		params: { pokemonId }
	} = useRouteMatch()

	useEffect(() => {
		onPokemonSelected(pokemonId)

		return () => { onUnmount() }
	}, [])

	if (isFetching) return pug`div.loading Loading...`

	if (fetchErrorMsg !== '') return pug`div.error ${fetchErrorMsg}`

	return pug`
		BackButton
		div#info
			div.row
				Pokemon(data=data)
				Stats(data=data.normalizedStats)
			div.row
				Types(data=data.normalizedTypes)
				Abilities(data=data)
	`
}

export default React.memo(PokemonInfo)
