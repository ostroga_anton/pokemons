import { Route, Switch } from 'react-router-dom'

import React from 'react'
import Main from './scenes/Main'
import PokemonAbility from './scenes/PokemonAbility'
import PokemonInfo from './scenes/PokemonInfo'

const App = () => {
	return pug`
		Switch
			Route(exact path='/' component=Main)
			Route(exact path='/pokemon/:pokemonId' component=PokemonInfo)
			Route(exact path='/pokemon/:pokemonId/ability/:abilityId' component=PokemonAbility)
		`
}

export default App
