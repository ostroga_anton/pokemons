import React, { useEffect } from 'react'
import PokemonList from './PokemonList'

const Main = ({
	onMounted,
	pokemons,
	onInputName,
	fetchErrorMsg,
	onUnmount,
	isFetching
}) => {
	useEffect(() => {
		onMounted()

		return () => { onUnmount() }
	}, [])

	if (isFetching) return pug`div.loading Loading...`

	if (fetchErrorMsg !== '') return pug`div.error #{fetchErrorMsg}`

	return pug`
		div#main-page
			input(onInput=onInputName type='text' placeholder='Enter pokemon name...')
			div#collection
				if pokemons
					PokemonList(items=pokemons)
	`
}

export default React.memo(Main)
