import React from 'react'
import typeColors from 'type-colors'

const Types = ({ data }) => {
	let i = 0

	return pug`
		div.types-wrapper Types:
			while i < data.length
				div.type(key=${`type-${i}`} style=${{ backgroundColor: typeColors[data[i]] }}) ${data[i++]}
	`
}

export default React.memo(Types)
