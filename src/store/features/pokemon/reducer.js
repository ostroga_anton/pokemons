import { handleActions } from 'redux-actions'
import {
	setPokemonData,
	clearPokemonData,
	setPokemonFetchingStatus,
	setPokemonFetchErrorMsg
} from './actions'

const initialState = {
	data: {},
	isFetching: true,
	fetchErrorMsg: ''
}

export default handleActions({
	[setPokemonData]: (state, { payload }) => {
		return {
			...state,
			data: payload
		}
	},
	[clearPokemonData]: () => {
		return {
			...initialState
		}
	},
	[setPokemonFetchingStatus]: (state, { payload }) => {
		return {
			...state,
			isFetching: payload
		}
	},
	[setPokemonFetchErrorMsg]: (state, { payload }) => {
		return {
			...state,
			fetchErrorMsg: payload
		}
	}
}, initialState)
