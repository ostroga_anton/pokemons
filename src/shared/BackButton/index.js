import React from 'react'
import { Link, useRouteMatch } from 'react-router-dom'

const BackButton = ({ currPage }) => {
	const {
		params: { pokemonId }
	} = useRouteMatch()

	let path = '/'

	if (currPage === 'ability-info') path = `/pokemon/${pokemonId}`

	return pug`
		div.back
			Link(to=path) < Prev Page
	`
}

export default React.memo(BackButton)
