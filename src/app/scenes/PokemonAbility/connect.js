import {
	clearAbilityData,
	getAbilityData,
	getAbilityFetchErrorMsg,
	getAbilityFetchingStatus,
	setCurrentAbility
} from 'store/features/ability'

import { connect } from 'react-redux'
import PokemonAbility from './component'

const mapStateToProps = (state) => {
	return {
		data: getAbilityData(state),
		fetchErrorMsg: getAbilityFetchErrorMsg(state),
		isFetching: getAbilityFetchingStatus(state)
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		onAbilitySelected: (id) => {
			dispatch(setCurrentAbility(id))
		},
		onUnmount: () => {
			dispatch(clearAbilityData())
		}
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(PokemonAbility)
