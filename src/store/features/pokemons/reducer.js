import { handleActions } from 'redux-actions'
import {
	setPokemonsData,
	inputName,
	setPokemonsFetchingStatus,
	setPokemonsFetchErrorMsg
} from './actions'

const initialState = {
	data: [],
	input: '',
	isFetching: true,
	fetchErrorMsg: ''
}

export default handleActions({
	[setPokemonsData]: (state, { payload }) => {
		return {
			...state,
			data: payload
		}
	},
	[inputName]: (state, action) => {
		return {
			...state,
			input: action.payload
		}
	},
	[setPokemonsFetchingStatus]: (state, action) => {
		return {
			...state,
			isFetching: action.payload
		}
	},
	[setPokemonsFetchErrorMsg]: (state, action) => {
		return {
			...state,
			fetchErrorMsg: action.payload
		}
	}
}, initialState)
