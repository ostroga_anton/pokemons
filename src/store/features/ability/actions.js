import { createActions } from 'redux-actions'

export const {
	setAbilityData,
	clearAbilityData,
	setAbilityFetchingStatus,
	setAbilityFetchErrorMsg
} = createActions(
	'SET_ABILITY_DATA',
	'CLEAR_ABILITY_DATA',
	'SET_ABILITY_FETCHING_STATUS',
	'SET_ABILITY_FETCH_ERROR_MSG'
)

export const setCurrentAbility = (abilityId) => async (dispatch) => {
	try {
		dispatch(setAbilityFetchingStatus(true))

		const response = await fetch(`https://pokeapi.co/api/v2/ability/${abilityId}/`)
		const result = await response.json()
		const { effect_entries, name, id } = result
		const abilityData = { name, id }

		effect_entries.forEach((el) => {
			if (el.language.name === 'en') {
				abilityData.effect = el.effect
				abilityData.shortEffect = el.short_effect
			}
		})

		dispatch(setAbilityData(abilityData))
	} catch (error) {
		dispatch(setAbilityFetchErrorMsg('AbilityFetchErro'))
	} finally {
		dispatch(setAbilityFetchingStatus(false))
	}
}
