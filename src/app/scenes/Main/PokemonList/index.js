import React, { useEffect, useRef } from 'react'
import { useHistory } from 'react-router-dom'

import Pokemon from 'Pokemon'

const List = ({ items }) => {
	const history = useHistory()
	const wrapperRef = useRef()

	const clickHandler = (node) => {
		if (node === wrapperRef.current) return

		if (node.dataset.id) {
			history.push(`/pokemon/${node.dataset.id}`)
		} else {
			clickHandler(node.parentNode)
		}
	}

	useEffect(() => {
		wrapperRef.current.addEventListener('click', ({ target }) => {
			clickHandler(target)
		})
	}, [])

	let i = 0

	return pug`
		div(ref=wrapperRef)
			while i < items.length
				Pokemon(data=items[i] key=items[i++].id)
	`
}

export default React.memo(List)
