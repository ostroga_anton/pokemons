import { handleActions } from 'redux-actions'
import {
	setAbilityData,
	clearAbilityData,
	setAbilityFetchingStatus,
	setAbilityFetchErrorMsg
} from './actions'

const initialState = {
	data: {},
	isFetching: false,
	fetchErrorMsg: ''
}

export default handleActions({
	[setAbilityData]: (state, { payload }) => {
		return {
			...state,
			data: payload
		}
	},
	[clearAbilityData]: () => {
		return {
			...initialState
		}
	},
	[setAbilityFetchingStatus]: (state, { payload }) => {
		return {
			...state,
			isFetching: payload
		}
	},
	[setAbilityFetchErrorMsg]: (state, { payload }) => {
		return {
			...state,
			fetchErrorMsg: payload
		}
	}
}, initialState)
