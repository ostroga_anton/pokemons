import {
	clearPokemonData,
	getPokemonData,
	getPokemonFetchErrorMsg,
	getPokemonFetchingStatus,
	setCurrentPokemon
} from 'store/features/pokemon'

import { connect } from 'react-redux'
import PokemonInfo from './component'

const mapStateToProps = (state) => {
	return {
		data: getPokemonData(state),
		fetchErrorMsg: getPokemonFetchErrorMsg(state),
		isFetching: getPokemonFetchingStatus(state)
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		onPokemonSelected: (id) => { dispatch(setCurrentPokemon(id)) },
		onUnmount: () => { dispatch(clearPokemonData()) }
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(PokemonInfo)
