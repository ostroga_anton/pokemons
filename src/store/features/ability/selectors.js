export const getAbilityData = (state) => {
	const { data } = state.ability

	if (Object.keys(data).length !== 0) {
		return data
	}
}

export const getAbilityFetchErrorMsg = (state) => {
	const { fetchErrorMsg } = state.ability

	return fetchErrorMsg
}

export const getAbilityFetchingStatus = (state) => {
	const { isFetching } = state.ability

	return isFetching
}
