import React, { useEffect } from 'react'
import { useRouteMatch } from 'react-router-dom'
import BackButton from 'BackButton'

const PokemonAbility = ({
	data,
	onAbilitySelected,
	onUnmount,
	fetchErrorMsg,
	isFetching
}) => {
	const {
		params: { abilityId }
	} = useRouteMatch()

	useEffect(() => {
		onAbilitySelected(abilityId)

		return () => { onUnmount() }
	}, [abilityId])

	if (isFetching) return pug`div.loading Loading...`

	if (fetchErrorMsg !== '') return pug`div.error #{fetchErrorMsg}`

	return pug`
		BackButton(currPage='ability-info')
		div#ability-info
			if data
				div#ability-name
					b Name:
					span #{data.name}
				div#ability-effect
					b Effect:
					span #{data.effect}
				div#ability-short-effect
				b Short-effect:
				span #{data.shortEffect}

	`
}

export default React.memo(PokemonAbility)
