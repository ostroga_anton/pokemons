export const getPokemonData = (state) => {
	const { data } = state.pokemon

	if (Object.keys(data).length !== 0) {
		return data
	}
}

export const getPokemonFetchErrorMsg = (state) => {
	const { fetchErrorMsg } = state.pokemon

	return fetchErrorMsg
}

export const getPokemonFetchingStatus = (state) => {
	const { isFetching } = state.pokemon

	return isFetching
}
